import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import Constants from "expo-constants";

// Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDO_-63Ti00ZoFXpNF8MoyDmTKhXhfgEJc",
  authDomain: "usermail-firebase.firebaseapp.com",
  projectId: "usermail-firebase",
  storageBucket: "usermail-firebase.appspot.com",
  messagingSenderId: "215660861416",
  appId: "1:215660861416:web:175ed61f63df4b369cb65d",
};

console.log(Constants.manifest);

let Firebase;

if (firebase.apps.length === 0) {
  Firebase = firebase.initializeApp(firebaseConfig);
}

export default Firebase;
